import React, {useState} from 'react';
import {
  TextInput,
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {TextField} from 'react-native-material-textfield';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {StatusBarDark, StatusBarLight} from '../utils/CustomStatusBar';
import {globStyle, globalStyle, buttonStyle} from '../styles/style';
import {SubmitButton} from '../utils/Button';
import {useDispatch, useStore} from 'react-redux';
import {AsyncStorageSetUserId, GetProfileApi, SignInApi} from '../service/Api';
import * as actions from '../redux/actions';
import Loader from '../utils/Loader';

import CheckBox from '@react-native-community/checkbox';

const Login = ({navigation}) => {
  const [state, setState] = useState({
    // mobile: '6393953549',
    mobile: '8010478716',
    password: '123456',
    isLoading: false,
  });
  const dispatch = useDispatch();
  const store = useStore();
  const setLoading = (isLoading) => setState({...state, isLoading});
  const forgotHandler = () => navigation.navigate('ForgotPassword');

  const loginHandler = async () => {
    const alertMessage = (msg) =>
      alert(msg || 'Something went wrong Please try again');
    console.log('loginhandler');
    const {mobile, password} = state;
    if (mobile === '') {
      alert('Please enter mobile number or registered email address');
      return;
    }
    if (password === '') {
      alert('Please enter your password');
      return;
    }
    setLoading(true);
    const {deviceInfo} = store.getState();
    const body = {
      phone: mobile,
      password: password,
      device_id: deviceInfo.id,
      device_type: deviceInfo.os,
      device_token: deviceInfo.token,
    };

    // console.log(JSON.stringify(body, null, 2));
    const {status = false, user_detail = {}, msg} = await SignInApi(body);
    // setLoading(false);
    if (status && Object.keys(user_detail).length !== 0) {
      const user_id = +user_detail.user_id;
      if (user_id !== 0) {
        AsyncStorageSetUserId(user_id.toString());
        const {status = false, user_details = {}} = await GetProfileApi({
          user_id: user_id,
        });
        if (status && Object.keys(user_details).length !== 0) {
          dispatch(actions.Login(user_details));
          setLoading(false);
          navigation.reset({
            index: 0,
            routes: [{name: 'HomeScreen'}],
          });
        } else {
          setLoading(false);
        }
      } else {
        setLoading(false);
        alertMessage('Invalid Account');
      }
    } else {
      setLoading(false);
      alertMessage(msg);
    }
  };

  return (
    <SafeAreaProvider style={globalStyle.container}>
      <StatusBarDark />
      {state.loading && <Loader />}
      <TouchableOpacity style={globalStyle.skipTouch}>
        <Text style={globalStyle.skipText}>SKIP</Text>
      </TouchableOpacity>
      <Image
        source={require('../assets/bg/logo.png')}
        style={globalStyle.logo}
      />
      <Text style={globalStyle.titleText}>LOGIN</Text>
      <View style={styles.phoneView}>
        <TextField
          // autoFocus
          label="Mobile Number"
          value={state.mobile}
          prefix={'+91 '}
          keyboardType="phone-pad"
          labelFontSize={12}
          fontSize={16}
          lineWidth={0}
          activeLineWidth={0}
          baseColor={'#000000'}
          onChangeText={(text) => setState({...state, mobile: text})}
          tintColor={'#69707F'}
          textColor={'#1D1E2C'}
          baseColor={'#1D1E2C'}
          labelTextStyle={{
            fontFamily: 'Nunito-Regular',
            fontWeight: '400',
          }}
          containerStyle={{
            paddingHorizontal: 20,
          }}
          affixTextStyle={{
            paddingBottom: 4,
          }}
          maxLength={10}
        />
      </View>
      <View style={styles.phoneView}>
        <TextField
          // autoFocus
          secureTextEntry
          label="Password"
          value={state.password}
          prefix={''}
          keyboardType="visible-password"
          labelFontSize={12}
          fontSize={16}
          lineWidth={0}
          activeLineWidth={0}
          baseColor={'#000000'}
          onChangeText={(text) => setState({...state, password: text})}
          tintColor={'#69707F'}
          textColor={'#1D1E2C'}
          baseColor={'#1D1E2C'}
          labelTextStyle={{
            fontFamily: 'Nunito-Regular',
            fontWeight: '400',
          }}
          containerStyle={{
            paddingHorizontal: 20,
          }}
          affixTextStyle={{
            paddingBottom: 4,
          }}
          maxLength={10}
        />
      </View>
      <View style={styles.checkView}>
        <CheckBox
          value={state.check}
          tintColors={{true: '#E60379', false: 'black'}}
          onValueChange={(newValue) => setState({...state, check: newValue})}
        />
        <View style={styles.checkView_2}>
          <Text style={styles.checkText_1}>
            {`By Clicking Continue, you agree to `}
            <Text style={styles.checkText_2}>{`Our Terms `}</Text>
            {`and `}
            <Text style={styles.checkText_2}>{`Privacy Police`}</Text>
          </Text>
        </View>
      </View>
      <TouchableOpacity style={buttonStyle.touch} onPress={loginHandler}>
        <Text style={buttonStyle.title}>LOGIN</Text>
      </TouchableOpacity>
      {/* <View style={styles.orView}>
        <View style={styles.orViewHz} />
        <Text style={styles.orText}>Or</Text>
        <View style={styles.orViewHz} />
      </View>
      <View style={styles.socialView}>
        <TouchableOpacity style={styles.soucialViewTouchFacebook}>
          <Image
            source={require('../assets/icons/facebook.png')}
            style={styles.socialIcon}
          />
          <Text style={styles.socialTextFacebook}>Facebook</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.soucialViewTouchGoogle}>
          <Image
            source={require('../assets/icons/google.png')}
            style={styles.socialIcon}
          />
          <Text style={styles.socialTextGoogle}>Google</Text>
        </TouchableOpacity>
      </View> */}
      {/* <TouchableOpacity
        style={globalStyle.accountTouch}
        // onPress={navHandler}
      >
        <Text style={globalStyle.accountText_1}>
          {`Don't have an account?`}
          <Text style={globalStyle.accountText_2}>{` Sign Up Now`}</Text>
        </Text>
      </TouchableOpacity> */}
    </SafeAreaProvider>
  );
};

export default Login;

const styles = StyleSheet.create({
  phoneView: {
    backgroundColor: '#F7F7FB',
    borderRadius: 4,
    marginTop: 20,
    width: '80%',
  },

  loginTouch: {
    backgroundColor: '#E60379',
    width: '80%',
    borderRadius: 25,
    marginTop: 'auto',
    marginBottom: 10,
    paddingVertical: 10,
  },
  loginText: {
    fontSize: 18,
    color: '#FFFFFF',
    fontFamily: 'Nunito-SemiBold',
    textAlign: 'center',
  },

  checkView: {
    marginTop: 30,
    flexDirection: 'row',
    width: '80%',
  },
  checkView_2: {
    marginLeft: 20,
    marginRight: 30,
  },
  checkText_1: {
    fontFamily: 'Nunito-SemiBold',
    fontSize: 13,
    color: '#6F6F7B',
  },
  checkText_2: {
    color: '#E60379',
  },

  orView: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: '60%',
    marginVertical: 5,
  },
  orViewHz: {
    backgroundColor: '#DDE5ED',
    flex: 1,
    height: 1,
  },
  orText: {
    fontSize: 14,
    fontFamily: 'Nunito-SemiBold',
    color: '#454555',
  },
  socialView: {
    flexDirection: 'row',
    marginBottom: 10,
    width: '80%',
    paddingVertical: 10,
    justifyContent: 'space-around',
  },
  socialIcon: {
    height: 17,
    width: 17,
    resizeMode: 'contain',
  },
  socialTextFacebook: {
    fontSize: 18,
    fontFamily: 'Nunito-SemiBold',
    fontWeight: '600',
    color: '#FFFFFF',
  },
  socialTextGoogle: {
    fontSize: 18,
    fontFamily: 'Nunito-SemiBold',
    fontWeight: '600',
    color: '#000000cc',
  },
  soucialViewTouchFacebook: {
    backgroundColor: '#3B5998',
    borderRadius: 24,
    paddingVertical: 8,
    width: 135,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    shadowColor: '#3B5998',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
  soucialViewTouchGoogle: {
    backgroundColor: '#FFFFFF',
    borderRadius: 24,
    paddingVertical: 8,
    width: 135,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
});
