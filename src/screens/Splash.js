import React, {useEffect} from 'react';
import {ImageBackground, Image, View, StyleSheet} from 'react-native';
import {useDispatch} from 'react-redux';
import {
  AsyncStorageGetUserId,
  GetProfileApi,
  CheckStatusApi,
} from '../service/Api';
import * as actions from '../redux/actions';
import {StatusBarLight} from '../utils/CustomStatusBar';

const Splash = ({navigation}) => {
  const dispatch = useDispatch();
  const screenHandler = async () => {
    const user_id = await AsyncStorageGetUserId();
    console.log(user_id);
    if (user_id && user_id !== '') {
      console.log('pass');
      const {status = false, user_details = {}} = await GetProfileApi({
        user_id: user_id,
      });
      if (status && Object.keys(user_details).length !== 0) {
        console.log(JSON.stringify(user_details, null, 2));
        statusUpdate(user_id);
        dispatch(actions.Login(user_details));
        navigation.replace('HomeScreen');
        return;
      }
    }
    navigation.replace('Login');
  };
  const statusUpdate = async (user_id) => {
    const {app_status = '0', approved = '0'} = await CheckStatusApi({user_id});
    dispatch(
      actions.statusUpdate({
        app_status: app_status === '1',
        approved: approved === '1',
      }),
    );
  };
  useEffect(() => {
    setTimeout(() => {
      screenHandler();
    }, 2000);
  }, []);
  return (
    <View style={styles.container}>
      <StatusBarLight />
      <ImageBackground
        source={require('../assets/bg/splash.png')}
        style={styles.bgImage}>
        <Image
          source={require('../assets/bg/splashLogo.png')}
          style={styles.iconImage}
        />
      </ImageBackground>
    </View>
  );
};

export default Splash;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bgImage: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconImage: {
    width: 200,
    height: 200,
    resizeMode: 'contain',
  },
});
