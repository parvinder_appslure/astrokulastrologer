import {
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  Image,
  FlatList,
} from 'react-native';

import React, {useState} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {globStyle} from '../styles/style';
import {StatusBarLight} from '../utils/CustomStatusBar';
import {SimpleHeader} from '../utils/Header';

const OnlinePuja = ({navigation}) => {
  const [state, setState] = useState({
    FlatListItems: [
      {
        key: '#1',

        profile: require('../assets/profile.png'),
        add1: require('../assets/profileimg.png'),
        name: 'Akshita Sharma',
        need: 'Shall we meet today?',
        time: '5 : 45 PM',

        id: '#123456',
        pujaBookedDateTime: '24/11/2020 - 03:30 PM',
        customerName: 'Akshita Sharma',
        pujaDateTime: 'Match Making Report',
        pujaType: 'Brother',
        commissionEarn: ' ₹1000',
      },

      {
        key: '#2',

        id: '#123456',
        pujaBookedDateTime: '24/11/2020 - 03:30 PM',
        customerName: 'Akshita Sharma',
        pujaDateTime: 'Match Making Report',
        pujaType: 'Brother',
        commissionEarn: ' ₹1000',
      },

      {
        key: '#3',

        id: '#123456',
        pujaBookedDateTime: '24/11/2020 - 03:30 PM',
        customerName: 'Akshita Sharma',
        pujaDateTime: 'Match Making Report',
        pujaType: 'Brother',
        commissionEarn: ' ₹1000',
      },

      {
        key: '#4',

        id: '#123456',
        pujaBookedDateTime: '24/11/2020 - 03:30 PM',
        customerName: 'Akshita Sharma',
        pujaDateTime: 'Match Making Report',
        pujaType: 'Brother',
        commissionEarn: ' ₹1000',
      },

      {
        key: '#5',

        id: '#123456',
        pujaBookedDateTime: '24/11/2020 - 03:30 PM',
        customerName: 'Akshita Sharma',
        pujaDateTime: 'Match Making Report',
        pujaType: 'Brother',
        commissionEarn: ' ₹1000',
      },

      {
        key: '#6',

        id: '#123456',
        pujaBookedDateTime: '24/11/2020 - 03:30 PM',
        customerName: 'Akshita Sharma',
        pujaDateTime: 'Match Making Report',
        pujaType: 'Brother',
        commissionEarn: ' ₹1000',
      },

      {
        key: '#7',

        id: '#123456',
        pujaBookedDateTime: '24/11/2020 - 03:30 PM',
        customerName: 'Akshita Sharma',
        pujaDateTime: 'Match Making Report',
        pujaType: 'Brother',
        commissionEarn: ' ₹1000',
      },
    ],
  });

  const renderItem1 = ({item, index}) => {
    // alert(JSON.stringify(item))
    return (
      <View
        style={{
          justifyContent: 'center',
          alignSelf: 'center',
          width: '90%',
          marginBottom: 10,
        }}>
        <View
          style={{
            height: 185,
            width: '100%',
            backgroundColor: '#FFF',
            borderRadius: 8,
            marginTop: 20,
            elevation: 12,
          }}>
          <View
            style={{
              width: '90%',
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignSelf: 'center',
              marginTop: 10,
            }}>
            <Text
              style={{
                lineHeight: 18,
                fontSize: 14,
                fontWeight: 'bold',
                color: '#000',
                fontFamily: 'Avenir',
              }}>
              {item.id}
            </Text>

            <Text
              style={{
                lineHeight: 16,
                fontSize: 12,
                fontWeight: 'normal',
                color: '#9F9F9F',
                fontFamily: 'Avenir',
              }}>
              {item.pujaBookedDateTime}
            </Text>
          </View>

          <View
            style={{
              width: '100%',
              borderColor: '#C8C8D3',
              borderWidth: 0.7,
              borderStyle: 'dashed',
              borderRadius: 0.1,
              marginTop: 10,
            }}></View>

          <View
            style={{
              width: '90%',
              alignSelf: 'center',
              justifyContent: 'center',
              flexDirection: 'column',
            }}>
            <View
              style={{
                marginTop: 12,
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                alignSelf: 'center',
              }}>
              <Text
                style={{
                  lineHeight: 16,
                  fontSize: 12,
                  fontWeight: 'normal',
                  color: '#83878E',
                  fontFamily: 'Avenir',
                }}>
                Customer Name
              </Text>

              <Text
                style={{
                  lineHeight: 18,
                  fontSize: 14,
                  fontWeight: 'bold',
                  color: '#000',
                  fontFamily: 'Avenir',
                }}>
                {item.customerName}
              </Text>
            </View>

            <View
              style={{
                marginTop: 11,
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                alignSelf: 'center',
              }}>
              <Text
                style={{
                  lineHeight: 16,
                  fontSize: 12,
                  fontWeight: 'normal',
                  color: '#83878E',
                  fontFamily: 'Avenir',
                }}>
                Report Type
              </Text>

              <Text
                style={{
                  lineHeight: 18,
                  fontSize: 14,
                  fontWeight: 'bold',
                  color: '#000',
                  fontFamily: 'Avenir',
                }}>
                {item.pujaDateTime}
              </Text>
            </View>

            <View
              style={{
                marginTop: 11,
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                alignSelf: 'center',
              }}>
              <Text
                style={{
                  lineHeight: 16,
                  fontSize: 12,
                  fontWeight: 'normal',
                  color: '#83878E',
                  fontFamily: 'Avenir',
                }}>
                Relationship
              </Text>
              <Text
                style={{
                  lineHeight: 18,
                  fontSize: 14,
                  fontWeight: 'bold',
                  color: '#000',
                  fontFamily: 'Avenir',
                }}>
                {item.pujaType}
              </Text>
            </View>
          </View>

          <View
            style={{
              width: '100%',
              borderColor: '#C8C8D3',
              borderWidth: 0.7,
              borderStyle: 'dashed',
              borderRadius: 0.1,
              marginTop: 13,
            }}></View>

          <View
            style={{
              marginTop: 11,
              width: '90%',
              flexDirection: 'row',
              marginLeft: 15,
            }}>
            <Text
              style={{
                lineHeight: 22,
                fontSize: 14,
                fontWeight: 'bold',
                color: '#000',
                fontFamily: 'Avenir',
              }}>
              {' '}
              Commission Earned:{' '}
            </Text>

            <Text
              style={{
                lineHeight: 22,
                fontSize: 14,
                fontWeight: 'normal',
                color: '#F87B00',
                fontFamily: 'Avenir',
              }}>
              {' '}
              {item.commissionEarn}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  const _keyExtractor = (item, index) => item.key;

  return (
    <SafeAreaProvider style={globStyle.safeAreaView}>
      <StatusBarLight />

      {SimpleHeader('Report', () => navigation.goBack())}

      <View
        style={{
          width: '100%',
          justifyContent: 'center',
          alignSelf: 'center',
        }}>
        <FlatList
          style={{width: '100%', marginBottom: 70}}
          data={state.FlatListItems}
          horizontal={false}
          showsVerticalScrollIndicator={false}
          keyExtractor={_keyExtractor}
          renderItem={renderItem1}
        />
      </View>
    </SafeAreaProvider>
  );
};

export default OnlinePuja;
