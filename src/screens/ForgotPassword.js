import React from 'react';
import {useState} from 'react';
import {Image, Text, TextInput, View} from 'react-native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {ForgotPasswordApi} from '../service/Api';
import {globStyle} from '../styles/style';
import {SubmitButton} from '../utils/Button';
import {StatusBarLight} from '../utils/CustomStatusBar';
import Loader from '../utils/Loader';

const ForgotPassword = ({navigation}) => {
  const [state, setState] = useState({
    isLoading: false,
    mobile: '6393953549',
    otp: Math.floor(1000 + Math.random() * 9000).toString(),
  });
  const toggleLoading = (isLoading) => setState({...state, isLoading});
  const submitHandler = async () => {
    if (state.mobile !== '') {
      const {mobile, otp} = state;
      const body = {
        email_phone: mobile,
        otp: otp,
      };
      console.log(body);
      toggleLoading(true);
      const {status = false, id = 0} = await ForgotPasswordApi(body);
      toggleLoading(false);
      if (status && id !== 0) {
        navigation.navigate('OtpScreen', {
          mobile,
          id,
          otp,
        });
      }
    }
  };
  return (
    <SafeAreaProvider style={globStyle.safeAreaView}>
      <StatusBarLight />
      {state.isLoading && <Loader />}
      <Image
        style={{
          height: 110,
          width: 145,
          marginTop: 35,
          justifyContent: 'center',
          alignSelf: 'center',
        }}
        source={require('../assets/splogo.png')}
      />

      <Text
        style={{
          marginLeft: 10,
          fontSize: 72,
          marginTop: 37,
          color: '#F7F7FB',
        }}>
        Forgot
      </Text>

      <KeyboardAwareScrollView>
        <View
          style={{
            width: '83%',
            borderRadius: 6,
            justifyContent: 'center',
            alignSelf: 'center',
            marginTop: 32,
            backgroundColor: '#F7F7FB',
            height: 54,
          }}>
          <View
            style={{
              width: '90%',
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <TextInput
              placeholder="Mobile Number/Email"
              keyboardType="default"
              value={state.mobile}
              onChangeText={(mobile) => setState({...state, mobile})}
            />
          </View>
        </View>
        <View
          style={{
            marginTop: '70%',
          }}>
          {SubmitButton('Login', submitHandler)}
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaProvider>
  );
};

export default ForgotPassword;
