import {
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  Image,
  TextInput,
  StyleSheet,
} from 'react-native';

import React, {useState} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {Dropdown} from 'react-native-material-dropdown-v2-fixed';
import {SimpleHeader} from '../utils/Header';
import {globStyle} from '../styles/style';
import {StatusBarLight} from '../utils/CustomStatusBar';
import {SubmitButton} from '../utils/Button';
import {useDispatch, useStore} from 'react-redux';
import {GetProfileApi, UpdateProfileApi} from '../service/Api';
import * as actions from '../redux/actions';
const EditProfileScreen = ({navigation}) => {
  const store = useStore();
  const {user} = store.getState();
  const dispatch = useDispatch();
  console.log(user);
  const [state, setState] = useState({
    mobile: user.mobile,
    dob: user.dob,
    consultType: '',
    skill: '',
    experience: '',
    name: user.name,
    email: user.email,
    gender: user.gender,
    service_offered: user.service_offered,
    specialization: user.specialization,
    language: user.language,
    experience: user.experience,
    isLoading: false,
  });

  const inputView = (
    label,
    key,
    placeholder,
    keyboardType = 'default',
    editiable = true,
  ) => (
    <>
      <Text style={styles.label}>{label}</Text>
      <View style={styles.outerView}>
        <TextInput
          style={styles.textInput}
          editable={editiable}
          keyboardType="default"
          placeholder={placeholder}
          keyboardType={keyboardType}
          onChangeText={(value) => setState({...state, [key]: value})}
          value={state[key]}
        />
        <Image
          source={require('../assets/edit1.png')}
          style={styles.editImage2}
        />
      </View>
    </>
  );
  const saveHandler = async () => {
    const {user_id} = user;
    const {
      service_offered,
      gender,
      name,
      dob,
      specialization,
      experience,
      language,
    } = state;
    const body = {
      user_id,
      name,
      dob,
      gender,
      service_offered,
      specialization,
      language,
      experience,
    };
    if (body.name === '') {
      alert('Please Enter Your Name');
      return;
    }
    if (body.dob === '') {
      alert('Please Enter Your Dob (YYYY-MM-DD)');
      return;
    }
    if (body.gender === '') {
      alert('Please Enter Your Gender (Male or Female)');
      return;
    }
    if (body.service_offered === '') {
      alert('Please Enter Your Consultand Type');
      return;
    }
    if (body.specialization === '') {
      alert('Please Enter Your Skills');
      return;
    }
    if (body.experience === '') {
      alert('Please Enter Your Experience in Years');
      return;
    }
    setState({...state, isLoading: true});
    const {status = false} = await UpdateProfileApi(body);
    setState({...state, isLoading: false});
    if (status) {
      updateProfile(body.user_id);
      alert('Your Profile has been Updated Successfull');
    } else {
      alert('Something went wrong');
    }
  };
  const updateProfile = async (user_id) => {
    const {status = false, user_details = {}} = await GetProfileApi({
      user_id: user_id,
    });
    if (status) {
      dispatch(actions.Login(user_details));
    }
  };
  return (
    <SafeAreaProvider style={globStyle.safeAreaView}>
      <StatusBarLight />
      {SimpleHeader('Edit Profile', () => navigation.goBack())}

      <View style={styles.container}>
        <KeyboardAwareScrollView>
          {inputView('Name', 'name')}
          {inputView('Email', 'email', '', '', false)}
          {inputView('Mobile', 'mobile', '', 'number-pad', false)}
          {inputView('Gender', 'gender', 'Male or Female', '')}
          {inputView('D.O.B', 'dob', 'DD-MM-YYYY', 'number-pad')}
          {inputView('Consultand Type', 'service_offered')}
          {inputView('Skill', 'specialization')}
          {inputView('Language (Hindi|English)', 'language', 'Hindi|English')}
          {inputView('Experience (in years)', 'experience')}
          {SubmitButton('SAVE', saveHandler)}
        </KeyboardAwareScrollView>
      </View>
    </SafeAreaProvider>
  );
};

export default EditProfileScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginVertical: 20,
  },
  label: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 20,
    color: '#69707F',
    marginHorizontal: 30,
  },
  textInput: {
    flex: 1,
    fontFamily: 'Avenir-Medium',
    fontSize: 20,
    fontWeight: '500',
    color: '#000000',
    alignSelf: 'center',
  },
  outerView: {
    flexDirection: 'row',
    borderBottomColor: '#EAEAEA',
    borderBottomWidth: 1,
    marginBottom: 20,
    width: '82%',
    alignSelf: 'center',
  },
  editImage2: {
    height: 15,
    width: 15,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginHorizontal: 5,
  },
});
