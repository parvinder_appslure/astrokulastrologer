import {
  ScrollView,
  View,
  Text,
  TouchableOpacity,
  Image,
  ImageBackground,
  StyleSheet,
  Dimensions,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';
import {Switch} from 'react-native-switch';
const {width, height} = Dimensions.get('window');
import React, {useEffect, useState} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import Global from './Global';

import {globStyle} from '../styles/style';
import {StatusBarLight} from '../utils/CustomStatusBar';
import Loader from '../utils/Loader';
import {useDispatch, useSelector, useStore} from 'react-redux';
import * as actions from '../redux/actions';
import {DynamicApi, HomeApi, StatusApi} from '../service/Api';
import {useIsFocused} from '@react-navigation/native';

const HomeScreen = ({navigation}) => {
  const store = useStore();
  const isFocused = useIsFocused();
  const [state, setState] = useState({
    video: false,
    audio: false,
    chat: false,
    isLoading: false,
    user_id: store.getState().user.user_id,
    app_status: store.getState().app_status,
    approved: store.getState().approved,
    user_detail: [],
  });
  const {netInfo, user} = useSelector((store) => store);
  const dispatch = useDispatch();

  const headerView = () => (
    <LinearGradient
      colors={['#E20277', '#6100A5']}
      start={{x: 1, y: 1}}
      end={{x: 0, y: 0}}
      style={styles.header_container}>
      <View style={styles.header_icon_view}>
        <Image
          style={styles.header_icon}
          source={require('../assets/bg/logo.png')}
        />
      </View>
      <View style={styles.header_titleView}>
        <Text style={styles.header_text_1}>Welcome</Text>
        <Text style={styles.header_text_2}>{user.name}</Text>
      </View>
      <TouchableOpacity style={styles.header_notifyTouch}>
        <Image
          style={styles.header_notifyIcon}
          //   style={{height: 26, width: 23, resizeMode: 'contain'}}
          source={require('../assets/notificationBell.png')}
        />
      </TouchableOpacity>
    </LinearGradient>
  );
  const statusView = () => (
    <View style={styles.sv_container}>
      <View style={styles.sv_h1}>
        <Text style={styles.sv_t1}>Type</Text>
        <Text style={styles.sv_t1}>Status</Text>
      </View>
      <View style={styles.sv_h1}>
        <Text style={styles.sv_t2}>Video Call</Text>
        {switchView('video')}
      </View>
      <View style={styles.sv_h1}>
        <Text style={styles.sv_t2}>Phone Call</Text>
        {switchView('audio')}
      </View>
      <View style={styles.sv_h1}>
        <Text style={styles.sv_t2}>Chat</Text>
        {switchView('chat')}
      </View>
    </View>
  );
  const bookingView = () => {
    const {user_detail} = state;
    return (
      <>
        <Text style={styles.bv_title}>New Booking</Text>
        <View style={styles.bv_container}>
          <View style={styles.bv_h1}>
            <Text style={styles.sv_t1}>Date: 26/11/2020</Text>
            <Text style={styles.sv_t1}>Time: 2:00 PM</Text>
          </View>
          <View style={styles.bv_h2}>
            <Image
              style={styles.bv_image}
              source={{uri: user_detail.user_image_path + user_detail.image}}
            />
            <View style={styles.bv_h3}>
              <Text style={styles.bv_t1}>{user_detail.name}</Text>
              <Text
                style={styles.bv_t2}>{`${user_detail.gender} | 25 yrs`}</Text>
              <Text style={styles.bv_t3}>Call: Video Call</Text>
            </View>
            <View style={styles.bv_h4}>
              <TouchableOpacity style={styles.bv_touch_accept}>
                <Text style={styles.bv_t4}>ACCEPT</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.bv_touch_decline}>
                <Text style={styles.bv_t4}>DECLINE</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </>
    );
  };
  const optionView = (title, source, key) => (
    <TouchableOpacity
      style={styles.opt_touch}
      onPress={() => optionHandler(key)}>
      {/* <ImageBackground
        style={styles.opt_bg}
        source={require('../assets/background.png')}> */}
      <Image source={source} style={styles.opt_image} />
      <Text style={styles.opt_title}>{title}</Text>
      {/* </ImageBackground> */}
    </TouchableOpacity>
  );
  const switchView = (key) => (
    <Switch
      barHeight={18}
      value={state[key]}
      onValueChange={(value) => updateStatus(key, value)}
      circleSize={18}
      circleBorderWidth={0.5}
      innerCircleStyle={styles.switch_icStyle}
      switchLeftPx={2}
      switchRightPx={2}
      switchWidthMultiplier={2.5}
      switchBorderRadius={20}
      activeTextStyle={styles.switch_text}
      inactiveTextStyle={styles.switch_text}
    />
  );

  const updateStatus = async (key, value) => {
    toggleLoading(true);
    const {status = false} = await StatusApi({
      user_id: state.user_id,
      for: key,
      what: value ? '1' : '0',
    });
    if (status) {
      setState({...state, [key]: value, isLoading: false});
    } else {
      toggleLoading(false);
      alert('Something went wrong Please try again');
    }
  };
  const toggleLoading = (isLoading) => setState({...state, isLoading});
  const optionHandler = (key) => {
    console.log('key', key);
    switch (key) {
      case 'profile':
        navigation.navigate('ProfileScreen');
        break;
      case 'astrologer':
        navigation.navigate('PremiumAstrologer');
        break;
      case 'chat':
        navigation.navigate('ChatHistory');
        break;
      case 'setting':
        navigation.navigate('SettingScreen');
        break;
      case 'earning':
        navigation.navigate('TotalEarning');
        break;
      case 'horoscope':
        navigation.navigate('HoroscopeOrderedScreen');
        break;
      case 'pooja':
        navigation.navigate('OnlinePuja');
        break;
      case 'call':
        navigation.navigate('CallHistory');
        break;
      case 'video':
        navigation.navigate('VideoCallHistory');
        break;
      case 'logout':
        logoutHandler();
        break;
      default:
        console.log('somthing missing in switch');
    }
  };
  const logoutHandler = () => {
    dispatch(actions.Logout());
    navigation.reset({
      index: 0,
      routes: [{name: 'Login'}],
    });
  };

  useEffect(() => {
    return;
    let timer;
    const callback = async () => {
      let ms = 30000;
      if (netInfo.isInternetReachable && state.app_status && state.approved) {
        const result = await DynamicApi({
          user_id: state.user_id,
        });
        if (result.status) {
          ms = 5000;
          if (result.is_booking === 1) {
            switch (result.booking_type) {
              case 'chat':
                Global.bookingid = result.booking_id;
                Global.another = result.user_id;
                Global.user_id = state.user_id;
                navigation.navigate('MyChat');
                break;
              case 'audio':
                break;
              case 'video':
                break;
            }
          }
          console.log(JSON.stringify(result, null, 2));
        }
      }
      timer = setTimeout(callback, ms);
    };
    if (isFocused) {
      console.log('focused');
      callback();
    } else {
      console.log('unfocused');
      clearTimeout(timer);
    }
    return () => {
      console.log('unmount main component');
      clearTimeout(timer);
    };
  }, [isFocused]);

  useEffect(() => {
    let timer;
    const callback = async () => {
      let ms = 30000;
      if (netInfo.isInternetReachable && state.app_status && state.approved) {
        const {status = false, user_detail = []} = await HomeApi({
          user_id: state.user_id,
        });
        if (status) {
          ms = 5000;
          // console.log(JSON.stringify(user_detail, null, 2));
          setState({...state, user_detail});
        }
      }
      timer = setTimeout(callback, ms);
    };
    if (isFocused) {
      console.log('focused');
      callback();
    } else {
      console.log('unfocused');
      clearTimeout(timer);
    }
    return () => {
      console.log('unmount main component');
      clearTimeout(timer);
    };
  }, [isFocused]);

  return (
    <SafeAreaProvider style={globStyle.safeAreaView}>
      <StatusBarLight />
      {state.isLoading && <Loader />}
      {headerView()}
      <ScrollView>
        {statusView()}
        {state.user_detail.length !== 0 && bookingView()}
        <View style={styles.opt_container}>
          {optionView(
            'My Profile',
            require('../assets/profileimg.png'),
            'profile',
          )}
          {/* {optionView(
            'Astrologer',
            require('../assets/calen.png'),
            'astrologer',
          )} */}
          {optionView('Chat', require('../assets/chat.png'), 'chat')}
          {optionView('Call', require('../assets/call.png'), 'call')}
          {optionView('Video Call', require('../assets/video.png'), 'video')}
          {optionView('Report', require('../assets/onlinePuja.png'), 'pooja')}
          {optionView(
            'Post Blog',
            require('../assets/horoscope.png'),
            'horoscope',
          )}
          {optionView(
            'Total Earning',
            require('../assets/total.png'),
            'earning',
          )}
          {optionView('Setting', require('../assets/settings.png'), 'setting')}
          {optionView('Logout', require('../assets/logout.png'), 'logout')}
        </View>
      </ScrollView>
    </SafeAreaProvider>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  header_container: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 20,
    paddingTop: 50,
  },
  header_icon: {
    height: 40,
    width: 40,
    resizeMode: 'contain',
  },
  header_icon_view: {
    backgroundColor: '#fff',
    borderRadius: 8,
    padding: 5,
  },
  header_titleView: {
    paddingHorizontal: 10,
  },
  header_text_1: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#FFFFFF',
  },
  header_text_2: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 16,
    color: '#FFFFFF',
    textTransform: 'uppercase',
  },
  header_notifyTouch: {
    marginLeft: 'auto',
  },
  header_notifyIcon: {
    height: 24,
    width: 24,
    resizeMode: 'contain',
  },
  sv_container: {
    margin: 20,
    padding: 15,
    paddingVertical: 5,
    borderRadius: 10,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  sv_h1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 5,
  },
  sv_t1: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 15,
    color: '#000000',
  },
  sv_t2: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 15,
    color: '#687080',
  },
  bv_title: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#1D1E2C',
    marginHorizontal: 20,
  },
  bv_container: {
    margin: 20,
    paddingVertical: 5,
    borderRadius: 10,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  bv_h1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
    paddingHorizontal: 15,
    borderBottomWidth: 1,
    borderColor: '#9797971a',
  },
  bv_h2: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 15,
  },
  bv_h3: {
    paddingHorizontal: 15,
  },
  bv_h4: {
    marginLeft: 'auto',
  },
  bv_image: {
    width: 60,
    height: 60,
    borderRadius: 30,
    resizeMode: 'contain',
  },
  bv_touch_accept: {
    backgroundColor: '#00B05F',
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 5,
    marginVertical: 4,
  },
  bv_touch_decline: {
    backgroundColor: '#ff3a31',
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 5,
    marginVertical: 4,
  },
  bv_t1: {
    fontFamily: 'Avenir-Heavy',
    fontWeight: '900',
    fontSize: 16,
    color: '#F97012',
  },
  bv_t2: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#69707F',
  },
  bv_t3: {
    fontFamily: 'Avenir-Medium',
    fontWeight: 'bold',
    fontSize: 14,
    color: '#212121',
  },
  bv_t4: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 12,
    color: '#FFFFFF',
  },
  opt_container: {
    margin: 20,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  opt_touch: {
    borderRadius: 15,
    marginHorizontal: 10,
    marginBottom: 5,
    width: (width - 100) / 3,
    height: 90,
    backgroundColor: 'white',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  opt_bg: {
    flex: 1,
    padding: 10,
    paddingTop: 20,
    justifyContent: 'space-between',
  },
  opt_image: {
    height: 24,
    width: 24,
    resizeMode: 'contain',
  },
  opt_title: {
    fontFamily: 'Avenir-Medium',
    fontWeight: '500',
    fontSize: 14,
    color: '#000000',
  },
  switch_icStyle: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  switch_text: {
    fontFamily: 'Avenir-Heavy',
    fontSize: 9,
    fontWeight: '900',
    color: '#fff',
  },
});
