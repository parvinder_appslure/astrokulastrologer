import {View, Text, StatusBar, TouchableOpacity} from 'react-native';

import React, {useState} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import StepIndicator from 'react-native-step-indicator';
import TextInput from 'react-native-material-textinput';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

const customStyles = {
  marginBottom: 10,
  stepIndicatorSize: 30,
  currentStepIndicatorSize: 30,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: '#F97012',
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: '#F97012',
  stepStrokeUnFinishedColor: '#e5e5e5',
  separatorFinishedColor: '#F97012',
  separatorUnFinishedColor: '#e5e5e5',
  stepIndicatorFinishedColor: '#F97012',
  stepIndicatorUnFinishedColor: '#e5e5e5',
  stepIndicatorCurrentColor: '#F97012',
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: '#F97012',
  stepIndicatorLabelFinishedColor: '#F97012',
  stepIndicatorLabelUnFinishedColor: 'black',
  labelColor: 'black',
  labelSize: 13,
  currentStepLabelColor: '#F97012',
};

const BankDetails = ({navigation}) => {
  const [state, setState] = useState({
    panCard: '',
    adharCard: '',
    BankName: '',

    accountHolderName: '',
    accountNumber: '',
    accountType: '',

    ifsc: '',
    gst: '',
  });

  return (
    <SafeAreaProvider style={{backgroundColor: 'white'}}>
      <StatusBar backgroundColor="orange" />

      <KeyboardAwareScrollView>
        <View style={{marginTop: 40}}>
          <StepIndicator
            customStyles={customStyles}
            //  currentPosition={parseInt(getStatus)}
            //  labels={labels}
            stepCount={4}
          />
        </View>

        <Text style={{fontSize: 51, marginTop: 42, color: '#F7F7FB'}}>
          REGISTRATION
        </Text>

        <View style={{width: '30%', marginLeft: 32}}>
          <Text
            style={{
              fontSize: 24,
              marginTop: -40,
              color: '#1D1E2C',
              fontFamily: 'Avenir',
              fontWeight: 'bold',
            }}>
            Bank Details
          </Text>
        </View>

        <View
          style={{
            width: '83%',
            borderRadius: 6,
            justifyContent: 'center',
            alignSelf: 'center',
            marginTop: 32,
            backgroundColor: '#F7F7FB',
            height: 54,
          }}>
          <View
            style={{
              width: '90%',
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <TextInput
              label="Pan Card No."
              numberOfLines={1}
              value={state.panCard}
              onChangeText={(panCard) => setState({...state, panCard})}
            />
          </View>
        </View>

        <View
          style={{
            width: '83%',
            borderRadius: 6,
            justifyContent: 'center',
            alignSelf: 'center',
            marginTop: 32,
            backgroundColor: '#F7F7FB',
            height: 54,
          }}>
          <View
            style={{
              width: '90%',
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <TextInput
              label="Aadhar Card"
              value={state.adharCard}
              numberOfLines={1}
              onChangeText={(adharCard) => setState({...state, adharCard})}
            />
          </View>
        </View>

        <View
          style={{
            width: '83%',
            borderRadius: 6,
            justifyContent: 'center',
            alignSelf: 'center',
            marginTop: 32,
            backgroundColor: '#F7F7FB',
            height: 54,
          }}>
          <View
            style={{
              width: '90%',
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <TextInput
              label="Bank Name"
              value={state.BankName}
              numberOfLines={1}
              onChangeText={(BankName) => setState({...state, BankName})}
            />
          </View>
        </View>

        <View
          style={{
            width: '83%',
            borderRadius: 6,
            justifyContent: 'center',
            alignSelf: 'center',
            marginTop: 32,
            backgroundColor: '#F7F7FB',
            height: 54,
          }}>
          <View
            style={{
              width: '90%',
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <TextInput
              label="Account Holder Name"
              value={state.accountHolderName}
              numberOfLines={1}
              onChangeText={(accountHolderName) =>
                setState({...state, accountHolderName})
              }
            />
          </View>
        </View>

        <View
          style={{
            width: '83%',
            borderRadius: 6,
            justifyContent: 'center',
            alignSelf: 'center',
            marginTop: 32,
            backgroundColor: '#F7F7FB',
            height: 54,
          }}>
          <View
            style={{
              width: '90%',
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <TextInput
              label="Account Number"
              value={state.accountNumber}
              numberOfLines={1}
              keyboardType={'numeric'}
              onChangeText={(accountNumber) =>
                setState(...state, {accountNumber})
              }
            />
          </View>
        </View>

        <View
          style={{
            width: '83%',
            borderRadius: 6,
            justifyContent: 'center',
            alignSelf: 'center',
            marginTop: 32,
            backgroundColor: '#F7F7FB',
            height: 54,
          }}>
          <View
            style={{
              width: '90%',
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <TextInput
              label="Account Type"
              value={state.accountType}
              numberOfLines={1}
              onChangeText={(accountType) => setState({...state, accountType})}
            />
          </View>
        </View>

        <View
          style={{
            width: '83%',
            borderRadius: 6,
            justifyContent: 'center',
            alignSelf: 'center',
            marginTop: 32,
            backgroundColor: '#F7F7FB',
            height: 54,
          }}>
          <View
            style={{
              width: '90%',
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <TextInput
              label="IFSC Code"
              value={state.ifsc}
              numberOfLines={1}
              onChangeText={(ifsc) => setState({...state, ifsc})}
            />
          </View>
        </View>

        <View
          style={{
            width: '83%',
            borderRadius: 6,
            justifyContent: 'center',
            alignSelf: 'center',
            marginTop: 32,
            backgroundColor: '#F7F7FB',
            height: 54,
          }}>
          <View
            style={{
              width: '90%',
              justifyContent: 'center',
              alignSelf: 'center',
            }}>
            <TextInput
              label="GST Number "
              value={state.gst}
              numberOfLines={1}
              onChangeText={(gst) => setState({...state, gst})}
            />
          </View>
        </View>

        <TouchableOpacity
          onPress={() => navigation.navigate('UploadDocuments')}>
          <View
            style={{
              width: '83%',
              marginBottom: 35,
              borderRadius: 25,
              justifyContent: 'center',
              alignSelf: 'center',
              marginTop: 30,
              backgroundColor: '#FA9219',
              height: 50,
            }}>
            <Text
              style={{
                lineHeight: 30,
                fontSize: 18,
                color: 'white',
                alignSelf: 'center',
              }}>
              Save & Next
            </Text>
          </View>
        </TouchableOpacity>
      </KeyboardAwareScrollView>
    </SafeAreaProvider>
  );
};

export default BankDetails;
