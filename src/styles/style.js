import {Platform, StatusBar, StyleSheet} from 'react-native';
export const globStyle = StyleSheet.create({
  safeAreaView: {
    backgroundColor: '#fff',
  },
});

export const globalStyle = {
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  container_2: {
    flex: 1,
    backgroundColor: 'white',
  },
  skipTouch: {
    padding: 5,
    marginTop: 20,
    marginBottom: 5,
    marginRight: 30,
    alignSelf: 'flex-end',
  },
  skipText: {
    color: '#6F6F7B',
    fontSize: 18,
    fontFamily: 'Nunito-SemiBold',
    fontWeight: '600',
  },
  logo: {
    height: 80,
    width: 80,
    resizeMode: 'contain',
  },
  titleText: {
    fontFamily: 'Nunito-Bold',
    fontSize: 30,
    fontWeight: '700',
    color: '#1D1E2C',
    marginHorizontal: 30,
    alignSelf: 'flex-start',
  },
  accountTouch: {
    marginBottom: 20,
    flexDirection: 'row',
    paddingHorizontal: 5,
  },
  accountText_1: {
    fontFamily: 'Nunito-SemiBold',
    fontSize: 14,
    fontWeight: '600',
    color: '#000521',
  },
  accountText_2: {
    color: '#E60379',
  },
};
export const headerStyle = StyleSheet.create({
  view: {
    flexDirection: 'row',
    paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight + 10 : 10,
    paddingBottom: 10,
    paddingHorizontal: 15,
    alignItems: 'center',
  },
  title: {
    fontFamily: 'Nunito-Bold',
    fontWeight: '700',
    fontSize: 20,
    color: '#FFFFFF',
  },
  backTouch: {
    marginRight: 10,
    paddingHorizontal: 5,
  },
  backImage: {
    height: 22,
    width: 22,
    resizeMode: 'contain',
  },
});

export const buttonStyle = StyleSheet.create({
  touch: {
    backgroundColor: '#E60379',
    width: '80%',
    borderRadius: 25,
    marginTop: 'auto',
    marginBottom: 40,
    paddingVertical: 10,
  },
  title: {
    fontSize: 18,
    color: '#FFFFFF',
    fontFamily: 'Nunito-SemiBold',
    textAlign: 'center',
  },
});
