import {create} from 'apisauce';
import {ApiSauceJson, ApiSauceJsonMulitpart} from './Config';
const ApiSauce = create(ApiSauceJson);
const ApiSauceMultiPart = create(ApiSauceJsonMulitpart);

export const request = (path, json) => {
  return new Promise((resolve, reject) => {
    ApiSauce.post(path, json).then((response) => {
      if (response.ok) {
        resolve(response.data);
      } else {
        console.log(response.err);
        reject(response.err);
      }
    });
  });
};
export const requestMultipart = (path, form) => {
  return new Promise((resolve, reject) => {
    ApiSauceMultiPart.post(path, form).then((response) => {
      if (response.ok) {
        resolve(response.data);
      } else {
        reject(response);
      }
    });
  });
};
